<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function kedua()
    {
        return view('halaman.form');
    }

    public function send(Request $request)
    {
        $firstName = $request['fname'];
        $lastName = $request['lname'];

        return view('halaman.welcome2', ['firstName'=> $firstName, 'lastName'=> $lastName]);

    }
}
